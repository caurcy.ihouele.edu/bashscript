﻿# Django Project Automation Script

This bash script automates the process of creating a Django project and its applications. It generates the main project and multiple applications within it, saving you from repetitive manual steps.

## Usage

To use this script, follow these steps:

1. Ensure you have Bash and Python installed on your system.
2. Clone this repository to your local machine.
3. Open a terminal and navigate to the directory containing the script.
4. Make the script executable if necessary: `chmod +x create_django_project.sh`
5. Run the script with the main project name and application names as arguments:

```bash
./create_django_project.sh main_project_name app1 app2 app3

#!/bin/bash

#list of arguments
args=("$@")
directory=$HOME/CS50/LinkedIn/Bash
main_project=${args[0]}
#you need to write the path of your python environment
source $HOME/django-heroku/bin/activate
total=$#
#create your main project
(
cd $directory || exit
if [[ ! -d $main_project ]]; then
    django-admin startproject $main_project
	echo "$main_project created"
else
    echo "$main_project already exists"
fi
)

(
cd $directory/$main_project || exit
i=1
until [ $i -ge $total ]
do
  	if [[ ! -d ${args[$i]} ]]; then
    	django-admin startapp ${args[$i]}
		if [[ -d ${args[$i]} ]]; then
		       #add the apps to settings.py
			echo "${args[$i]} application created"
			settings=$main_project/settings.py
			sed "/django.contrib.staticfiles/a'${args[$i]}',"\
			$settings > temp.$$
            mv temp.$$ $settings
	               #include paths from apps
			url=$main_project/urls.py
			sed 's/\import path\b/&,include/'\
			$url > temp.$$
            mv temp.$$ $url
			sed "/admin.site.urls/apath('${args[$i]}',include('${args[$i]}.urls')),"\
			$url > temp.$$
            mv temp.$$ $url
		        #create a simple index views function rendering a HTML
			echo 'def index(request): 
	return render(request,"'${args[$i]}'/index.html")' >>${args[$i]}/views.py
	                #create path for the index views function
			echo "from django.urls import path
from . import views
urlpatterns=[
    path('',views.index,name='index'),
   ]" >${args[$i]}/urls.py
                        #create templates/appname folder
			mkdir -p ${args[$i]}/templates/${args[$i]}
			#create a simple html page with a css file included
			echo "{% load static %}
				  <!DOCTYPE html>
				 <html lang="en">
    				<head>
        				<title>Hello!</title>
						<link href=\"{% static '${args[$i]}/styles.css' %}\" rel="stylesheet" />
    				</head>
    				<body>
        				Hello, world!
    				</body>
				</html>">${args[$i]}/templates/${args[$i]}/index.html
			#create static folder
			mkdir -p ${args[$i]}/static/${args[$i]}
			#create an empty css file to be edited after
			touch ${args[$i]}/static/${args[$i]}/styles.css
		fi
  	else
    	echo "${args[$i]} already exists"
	fi
  	((i=i+1))
done
)
  
````

Running the command below from your terminal to run the script.

````bash

#to make it executable
chmod +x createDjango.sh
./createDjango.sh myproject app1
````

- Run the server locally with the command below in the main project directory and see in your browser.

````bash
python manage.py runserver
````

When you go to the link from your browser, http://127.0.0.1:8000/app1, the HTML file is ultimately rendered.