﻿# Generate your Typescript project with this bash script

````bash
#!/bin/bash
# Initialize NPM project
mkdir typescript-project
cd typescript-project
git init
touch README.md
touch .gitignore
echo 'node_modules' > .gitignore
echo 'dist' >> .gitignore
npm init -y
# Install TypeScript
npm install typescript -g
npm install typescript --save-dev
# Create tsconfig.json
tsc --init

# Create index.ts
mkdir src
touch src/index.ts
echo 'console.log("Hello World!");' > src/index.ts
# Compile
tsc
echo "TypeScript project setup complete!"

````
To use it:

1. Save the script as init-typescript.sh
2. Make an executable with chmod +x init-typescript.sh
3. Run it via ./init-typescript.sh