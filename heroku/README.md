﻿## Explications

### Vérification de l'existence de l'application :

- L'URL de l'application est construite en combinant le nom de l'application fourni en argument avec le domaine Heroku.
- La commande `curl` est utilisée pour envoyer une requête HEAD à l'URL de l'application afin de vérifier son statut.
- Le code de réponse HTTP est extrait et stocké dans la variable `CURL_RESPONSE`.

### Création de l'application :

- Si le code de réponse est 404 (application non trouvée), cela signifie que l'application n'existe pas encore.
- Dans ce cas, une nouvelle application Heroku est créée à l'aide de la commande `heroku create`.
- Ensuite, les fichiers sont ajoutés à Git, un commit est effectué avec le message spécifié, et le code est poussé vers Heroku.

### Affichage des messages :

- Si l'application existe déjà (code de réponse différent de 404), un message indiquant que l'application existe déjà est affiché.

````bash
#!/bin/bash

# Données
APP_NAME=$1              # Nom de l'application Heroku
COMMIT_MESSAGE=$2        # Message de commit pour Git
HTTP='https://'          # Protocole pour l'URL de l'application
DOMAIN='.herokuapp.com'  # Domaine Heroku pour l'URL

# URL de l'application Heroku
REQ_URL="$HTTP$APP_NAME$DOMAIN"

# Affichage de l'URL de requête
echo "URL de requête : $REQ_URL"

# Vérification de l'existence de l'application
CURL_RESPONSE=$(curl -s -o /dev/null -w "%{http_code}" $REQ_URL)

# Affichage du code de réponse CURL
echo "Code de réponse CURL : $CURL_RESPONSE"

# Si l'application n'existe pas (code de réponse 404)
if [[ $CURL_RESPONSE -eq 404 ]]; then
    # Création de l'application Heroku
    heroku create $APP_NAME
    
    # Ajout de tous les fichiers à Git
    git add .
    
    # Commit des modifications avec le message spécifié
    git commit -m "$COMMIT_MESSAGE"
    
    # Pousser le code vers Heroku
    git push heroku master
else
    # Affichage du message si l'application existe déjà
    echo "L'application existe déjà."
fi

````